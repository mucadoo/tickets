# **Support Ticket System**

Here is how to run the applications.

**Requirements before run:**

1. Docker:
   - You will need to install the latest version of Docker and make sure that docker is started with no problems and ready to run containers. (https://www.docker.com/products/docker-desktop/)
2. Node.js:
    - You will need to install the latest LTS version of Node.js and make sure npm is working. (https://nodejs.org/en/download)
3. Ports:
   - The frontend uses port 3002, the backend 3000 and mongodb 27017. You will need to make sure that you have nothing running on theses ports in order to run the apps succesfully. 

**How to run**

- There are two folders (front and backend), each one with a script file to run the app through Docker.
- In order to run the backend, simple navigate to the folder via cmd (cd backend) and run the docker script (npm run docker).
- In order to run the front, simple navigate to the folder via cmd (cd frontend) and run the docker script (npm run docker) and wait until the app is compiled.
- Enter you browser and go to http://localhost:3002/ and you should be able to use the app
- Additionally, to see the documentation of the API ou can go to http://localhost:3000/v1/docs/
- I did not had time to split the font end in a more "componentized" way. But i added a few extras, hope it will cover the lacks. Hope you enjoy!