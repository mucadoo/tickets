const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { ticketService } = require('../services');

const createTicket = catchAsync(async (req, res) => {
  const ticket = await ticketService.createTicket(req.body);
  res.status(httpStatus.CREATED).send(ticket);
});

const getTickets = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['client', 'issue']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await ticketService.queryTickets(filter, options);
  res.send(result);
});

const updateTicket = catchAsync(async (req, res) => {
  const ticket = await ticketService.updateTicketById(req.params.ticketId, req.body);
  res.send(ticket);
});

module.exports = {
  createTicket,
  getTickets,
  updateTicket,
};
