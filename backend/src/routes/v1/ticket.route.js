const express = require('express');
const validate = require('../../middlewares/validate');
const ticketValidation = require('../../validations/ticket.validation');
const ticketController = require('../../controllers/ticket.controller');

const router = express.Router();

router
  .route('/')
  .post(validate(ticketValidation.createTicket), ticketController.createTicket)
  .get(validate(ticketValidation.getTickets), ticketController.getTickets);

router.route('/:ticketId').put(validate(ticketValidation.updateTicket), ticketController.updateTicket);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Tickets
 *   description: Ticket management and retrieval
 */

/**
 * @swagger
 * /tickets:
 *   post:
 *     summary: Create a ticket
 *     tags: [Tickets]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - client
 *               - issue
 *               - status
 *               - deadline
 *             properties:
 *               client:
 *                 type: string
 *               issue:
 *                 type: string
 *               status:
 *                 type: string
 *                 enum: [open, closed]
 *               deadline:
 *                  type: date
 *             example:
 *               client: Inflight Dublin
 *               issue: lorem ipsum lorem ipsum lorem ipsum lorem ipsum
 *               status: open
 *               deadline: '2024-02-15'
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Ticket'
 *
 *   get:
 *     summary: Get all tickets
 *     tags: [Tickets]
 *     parameters:
 *       - in: query
 *         name: client
 *         schema:
 *           type: string
 *         description: Ticket client
 *       - in: query
 *         name: status
 *         schema:
 *           type: string
 *         description: Ticket issue message
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. client:asc)
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *         description: Maximum number of tickets
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Ticket'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 */

/**
 * @swagger
 * /tickets/{id}:
 *   patch:
 *     summary: Update a ticket
 *     tags: [Tickets]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Ticket id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: enum
 *                 description: must be open or closed
 *             example:
 *               status: closed
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Ticket'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */
