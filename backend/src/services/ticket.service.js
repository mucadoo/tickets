const httpStatus = require('http-status');
const { Ticket } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a ticket
 * @param {Object} ticketBody
 * @returns {Promise<Ticket>}
 */
const createTicket = async (ticketBody) => {
  return Ticket.create(ticketBody);
};

/**
 * Query for tickets
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryTickets = async (filter, options) => {
  const tickets = await Ticket.paginate(filter, options);
  return tickets;
};

/**
 * Get ticket by id
 * @param {ObjectId} id
 * @returns {Promise<Ticket>}
 */
const getTicketById = async (id) => {
  return Ticket.findById(id);
};

/**
 * Update ticket by id
 * @param {ObjectId} ticketId
 * @param {Object} updateBody
 * @returns {Promise<Ticket>}
 */
const updateTicketById = async (ticketId, updateBody) => {
  const ticket = await getTicketById(ticketId);
  if (!ticket) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Ticket not found');
  }
  Object.assign(ticket, updateBody);
  await ticket.save();
  return ticket;
};

module.exports = {
  createTicket,
  queryTickets,
  updateTicketById,
};
