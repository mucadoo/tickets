const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createTicket = {
  body: Joi.object().keys({
    client: Joi.string().required(),
    issue: Joi.string().required(),
    status: Joi.string().required().valid('open', 'closed'),
    deadline: Joi.date().required(),
  }),
};

const getTickets = {
  query: Joi.object().keys({
    client: Joi.string(),
    issue: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const updateTicket = {
  params: Joi.object().keys({
    ticketId: Joi.required().custom(objectId),
  }),
  body: Joi.object().keys({
    status: Joi.string().required().valid('open', 'closed'),
  }),
};

module.exports = {
  createTicket,
  getTickets,
  updateTicket,
};
