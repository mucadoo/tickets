// eslint-disable-next-line import/no-extraneous-dependencies
const { faker } = require('@faker-js/faker');
const { Ticket } = require('../../../src/models');

describe('Ticket model', () => {
  describe('Ticket validation', () => {
    let newTicket;
    beforeEach(() => {
      newTicket = {
        client: faker.company.name(),
        issue: faker.lorem.paragraph(),
        status: faker.helpers.arrayElement(['open', 'closed']),
        deadline: faker.date.anytime(),
      };
    });

    test('should correctly validate a valid ticket', async () => {
      await expect(new Ticket(newTicket).validate()).resolves.toBeUndefined();
    });
  });
});
