import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('timeline header', () => {
  render(<App />);
  const linkElement = screen.getByText(/Timeline/i);
  expect(linkElement).toBeInTheDocument();
});
