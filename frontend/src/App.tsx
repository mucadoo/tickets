import React, { useEffect, useState } from 'react';
import {
  Card,
  CardActions,
  CardContent,
  Typography,
  GridListTile, Box, Modal, Switch, CircularProgress, FormControl, InputLabel, Select, MenuItem,
} from '@material-ui/core';
import CalendarToday from '@material-ui/icons/CalendarToday';
import RadioButtonChecked from '@material-ui/icons/RadioButtonChecked';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';

import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
// eslint-disable-next-line import/no-extraneous-dependencies
import { faker } from '@faker-js/faker';
import httpRequest from './services/httpRequest';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  gridList: {
    width: '100%',
    height: 'auto',
  },
  card: {
    maxWidth: 160,
    height: '100%',
  },
  customSwitch: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    zIndex: 1000000,
  },
}));

const CustomSwitch = withStyles({
  switchBase: {
    color: '#46C3B3',
    '&$checked': {
      color: '#B9B9B9',
    },
    '&$checked + $track': {
      backgroundColor: '#B9B9B9',
    },
  },
  checked: {},
  track: {},
})(Switch);

const LoadingOverlay = () => {
  const classes = useStyles();

  return (
    <div className={classes.overlay}>
      <CircularProgress color="primary" />
    </div>
  );
};

export default function App() {
  const classes = useStyles();
  const [openModal, setOpenModal] = useState(false);
  const [ticketsData, setTicketsData] = useState<any[]>([]);
  const [formData, setFormData] = useState({
    client: '',
    issue: '',
    status: '',
    deadline: '',
  });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getTickets();
  }, []);

  const getTickets = () => {
    setLoading(true);
    httpRequest
        .get('/tickets', { params: { limit: 1000 } })
        .then((response: any) => {
          setTicketsData(response.data.results);
        }).finally(() => {
          setLoading(false);
        });
  };

  const handleFieldChange = (event: { target: { name: any; value: any; }; }) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event: { preventDefault: () => void; }) => {
    event.preventDefault();
    setLoading(true);
    httpRequest.post('/tickets', formData)
        .then((response: any) => {
          getTickets();
        }).finally(() => {
          setLoading(false);
        });
  };

  const handleModalOpen = () => {
    setOpenModal(true);
  };

  const handleModalClose = () => {
    setOpenModal(false);
  };

  const handleChange = (event: any, id: any, currentStatus: any) => {
    const newStatus = currentStatus == 'open' ? 'closed' : 'open';
    setLoading(true);
    httpRequest.put(`/tickets/${id}`, {
      status: newStatus,
    }).then((response: any) => {
      const updatedTicketData = ticketsData.map((ticket:any) => {
        if (ticket.id == id) {
          return { ...ticket, status: newStatus };
        }
        return ticket;
      });
      setTicketsData(updatedTicketData);
    }).finally(() => {
      setLoading(false);
    });
  };

  const getStatusColor = (date: any, status: any): string => {
    if (status == 'closed'){
      return '#03C854';
    }
    const currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
    const givenDate = new Date(date.slice(0, 10));
    if (currentDate <= givenDate){
      return '#FEC107';
    }
    return '#D74315';
  };

  function formatDate(dateString: string) {
    const dateParts = dateString.slice(0, 10).split('-');
    const year = dateParts[0];
    const month = dateParts[1];
    const day = dateParts[2];
    const formattedDate = `${day}/${month}/${year}`;
    return formattedDate;
  }

  const createRandom = () => {
    const currentDate = new Date();
    const twoDaysBefore = new Date();
    twoDaysBefore.setDate(currentDate.getDate() - 2);
    const twoDaysAfter = new Date();
    twoDaysAfter.setDate(currentDate.getDate() + 2);
    const newTicket = {
      client: faker.company.name().slice(0, 20),
      issue: faker.lorem.paragraph(),
      status: faker.helpers.arrayElement(['open', 'closed']),
      deadline: faker.date.between({ from: `${twoDaysBefore.toISOString().split('T')[0]}T00:00:00.000Z`, to: `${twoDaysAfter.toISOString().split('T')[0]}T23:59:59.000Z` }),
    };
    setLoading(true);
    httpRequest.post('/tickets', newTicket)
        .then((response: any) => {
          getTickets();
        }).finally(() => {
      setLoading(false);
    });
  };

  const handleSelectChange = (event: { target: { value: any; }; }) => {
    setFormData({
      ...formData,
      status: event.target.value,
    });
  };

  return (
    <div className="App">
      {loading && <LoadingOverlay />}
      <Modal open={openModal} onClose={handleModalClose}>
        <div style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          backgroundColor: '#fff',
          padding: '20px',
          outline: 'none',
        }}
        >
          <form onSubmit={handleSubmit}>
            <TextField
              label="Client"
              fullWidth
              name="client"
              value={formData.client}
              onChange={handleFieldChange}
            />
            <TextField
              label="Issue message"
              fullWidth
              name="issue"
              value={formData.issue}
              onChange={handleFieldChange}
            />
            <FormControl fullWidth>
              <InputLabel>Status</InputLabel>
              <Select
                onChange={handleSelectChange}
                name="status"
                value={formData.status}
              >
                <MenuItem value="open">Open</MenuItem>
                <MenuItem value="closed">Closed</MenuItem>
              </Select>
            </FormControl>
            <TextField
              label="Deadline"
              fullWidth
              name="deadline"
              value={formData.deadline}
              onChange={handleFieldChange}
              placeholder='YYYY-MM-DD'
            />
            <Button type="submit" variant="contained" color="primary" style={{ textTransform: 'none', backgroundColor: '#3B8EDE' }}>Submit</Button>
          </form>
        </div>
      </Modal>
      <div style={{
        position: 'fixed',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid #D8D8D8',
        borderRadius: '10px',
        overflow: 'hidden',
        height: '93vh',
        width: '97%',
      }}
      >
        <div style={{
          position: 'fixed',
          top: '0',
          left: '0',
          right: '0',
          height: '90px',
          display: 'flex',
          alignItems: 'left',
          justifyContent: 'left',
          zIndex: '999',
        }}
        >
          <span style={{ margin: '20px' }}>
            <CalendarToday style={{ border: '2px solid #9DC7EF', borderRadius: '10px', padding: '10px', color: '#1D476F' }} />
          </span>
          <Typography variant="h6" style={{ margin: '25px' }}>
            Timeline
          </Typography>
        </div>
        <GridList cellHeight="auto" style={{ marginTop: '90px', height: 'calc(93vh - 90px)', display: 'block' }} cols={2} className={classes.gridList} spacing={0}>
          {ticketsData.map((ticket: any, index: any) => (
            <GridListTile key={ticket.id} style={{ margin: '15px', borderRadius: '10px' }}>
              <Card style={{ width: '500px', borderRadius: '10px', backgroundColor: '#D0E2F5' }}>
                <CardActions disableSpacing className={classes.customSwitch}>
                  <span>
                    <Typography component="span"><Box fontWeight='fontWeightMedium' display='inline'>{ index + 1 }.</Box></Typography>
                    <Typography component="span"><Box sx={{ ml: 2 }} fontWeight='fontWeightMedium' display='inline'>{ ticket.client }</Box></Typography>
                    <Typography component="span"><Box sx={{ ml: 7 }} fontWeight='fontWeightMedium' display='inline'>{ formatDate(ticket.deadline) }</Box></Typography>
                  </span>
                  <span>
                    <CustomSwitch
                      checked={ticket.status == 'closed'}
                      onChange={(event) => handleChange(event, ticket.id, ticket.status)}
                      inputProps={{ 'aria-label': 'controlled' }}
                    />
                    <IconButton style={{ color: getStatusColor(ticket.deadline, ticket.status), fontSize: 24 }}>
                      <RadioButtonChecked />
                    </IconButton>
                  </span>
                </CardActions>
                <CardContent style={{ padding: '10px', paddingTop: '0' }}>
                  <Typography variant="body2" color="textSecondary" component="p" style={{ borderRadius: '10px', padding: '15px', backgroundColor: 'white', wordBreak: 'break-all' }}>
                    { ticket.issue }
                  </Typography>
                </CardContent>
              </Card>
            </GridListTile>
          ))}
        </GridList>
        <Box
          style={{
              position: 'fixed',
              bottom: 0,
              left: 0,
              display: 'flex',
              justifyContent: 'flex-end',
              padding: '10px',
              backgroundColor: 'transparent',
              width: '-webkit-fill-available',
            }}
        >
          <Button disabled={loading} variant="contained" color="primary" style={{ marginRight: '10px',  textTransform: 'none', backgroundColor: '#3B8EDE' }} onClick={createRandom}>
            Create Ramdomly {'>'}
          </Button>
          <Button disabled={loading} variant="contained" color="secondary" style={{ marginRight: '10px',  textTransform: 'none', backgroundColor: '#3B8EDE' }} onClick={handleModalOpen}>
            Create New {'>'}
          </Button>
        </Box>
      </div>
    </div>
  );
}
