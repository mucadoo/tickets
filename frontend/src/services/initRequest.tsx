import axios, { AxiosRequestConfig } from 'axios';

export type IConfig = AxiosRequestConfig;

const requestConfig: IConfig = {
  baseURL: process.env.REACT_APP_ENDPOINT_URL,
  timeout: 5000,
};

export const axiosInstance = axios.create(requestConfig);
